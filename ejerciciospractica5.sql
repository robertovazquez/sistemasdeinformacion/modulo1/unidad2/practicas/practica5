﻿DROP DATABASE IF EXISTS practica5;
CREATE DATABASE practica5;
USE practica5;

CREATE OR REPLACE TABLE federacion(

nombre varchar (30),
direccion varchar (30),
telefono int,
PRIMARY KEY (nombre)

);

CREATE OR REPLACE TABLE mienbro (

  dni int,
  nombre_m varchar (30),
  titulacion varchar (30),
PRIMARY KEY (dni)

);

CREATE OR REPLACE TABLE composicion (

   nombrefe varchar (30),
   dnimi int,
   cargo varchar (30),
   fecha_inicio date,
  PRIMARY KEY (nombrefe,dnimi)

);

ALTER TABLE composicion ADD CONSTRAINT fkcomposfedera FOREIGN KEY (nombrefe) REFERENCES federacion (nombre);

ALTER TABLE composicion ADD CONSTRAINT fkcomposmien FOREIGN KEY (dnimi) REFERENCES mienbro (dni);



